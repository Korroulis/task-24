CREATE database `journal`;
USE `journal`;

CREATE TABLE `author` (
author_id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
full_name VARCHAR(128) NOT NULL,
age INT NOT NULL,
country VARCHAR(128) NOT NULL,
email VARCHAR(128) NOT NULL,
created_at DATETIME NOT NULL DEFAULT NOW(),
update_at DATETIME DEFAULT NULL);

CREATE TABLE `journal` (
journal_id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
title VARCHAR(128) NOT NULL,
author_name VARCHAR(128) NOT NULL,
category VARCHAR(128) NOT NULL,
created_at DATETIME NOT NULL DEFAULT NOW(),
update_at DATETIME DEFAULT NULL);

CREATE TABLE `entry` (
entry_id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
title VARCHAR(128) NOT NULL,
description_text VARCHAR(255) NOT NULL,
author_id INT(11) NOT NULL,
journal_id INT(11) NOT NULL,
created_at DATETIME NOT NULL DEFAULT NOW(),
update_at DATETIME DEFAULT NULL,
FOREIGN KEY(author_id) references author(author_id),
FOREIGN KEY(journal_id) references journal(journal_id));

CREATE TABLE `tag` (
tag_id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
title VARCHAR(128) NOT NULL,
created_at DATETIME NOT NULL DEFAULT NOW(),
update_at DATETIME DEFAULT NULL);

CREATE TABLE `entry_tag`(
   entry_id INT(11) NOT NULL,
   tag_id INT(11) NOT NULL,
   Primary Key (entry_id,tag_id), 
   Foreign Key (entry_id) REFERENCES entry(entry_id),
   Foreign Key (tag_id) REFERENCES tag(tag_id));

