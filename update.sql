UPDATE authors
SET email='dewaldif@gmail.com'
WHERE author_id=1;

UPDATE authors
SET full_name='P the great'
WHERE full_name='Vladimir P';

UPDATE authors
SET age='50'
WHERE full_name='Donald T';

UPDATE journals
SET category='science'
WHERE title='Nature';

UPDATE journals
SET category='comic'
WHERE title='Comicoooon';

UPDATE journals
SET category='fiction'
WHERE title='The earth is flat';

UPDATE entry
SET text='The earth if flat...'
WHERE title='Why the earth is flat';

UPDATE entry
SET category='science'
WHERE title='Nature';

UPDATE tag
SET title='app.js'
WHERE title='index.js';
