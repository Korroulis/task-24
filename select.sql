USE journal;

# sort author names and their respective age by age in descending order

SELECT full_name, age FROM author
ORDER BY age DESC;

# find the maximum age of all authors and the author with the maximum age

SELECT MAX(age) AS 'MAXIMUM AUTHOR AGE',full_name  AS 'AUTHOR NAME' FROM author WHERE age= (SELECT MAX(AGE) FROM author);

# find all the journals of category science

SELECT title from journal WHERE CATEGORY = 'science';

# list all the journals under the science category in the same row

  SELECT GROUP_CONCAT(title SEPARATOR ', ') AS 'Scientific journals' FROM journal WHERE category = 'science' ;
  
# find the journal coupled to the author with the shortest name/s (gaps and special characters in the name add to the length)

SELECT title FROM journal WHERE LENGTH(author_name)=(SELECT MIN(LENGTH(full_name)) FROM author);

# present entries together with the journal title where they appear 

SELECT entry_id, entry.title, journal.title AS 'journal_title'
FROM entry
INNER JOIN journal
ON journal.journal_id = entry.journal_id;

# Find the number of entries in 'The earth is flat' journal

SELECT entry.title AS 'entry_title',journal.title
FROM entry
INNER JOIN journal
ON journal.journal_id = entry.journal_id WHERE journal.title='The earth is flat';

# find all the entries from the youngest author

SELECT entry.title AS 'entry_title', author.full_name
FROM entry
INNER JOIN author
ON author.author_id = entry.author_id WHERE author.age= (SELECT MIN(AGE) FROM author);

# Return all the tags in the 'Why the earth is flat' entry

SELECT tag.title FROM entry
  LEFT JOIN entry_tag ON entry_tag.entry_id = entry.entry_id
  LEFT JOIN tag ON entry_tag.tag_id = tag.tag_id
  WHERE entry.title ='Why the earth is flat';

# Return all the tags in the latest created entry

SELECT tag.title AS 'tags_latest_created_entry' FROM entry
  LEFT JOIN entry_tag ON entry_tag.entry_id = entry.entry_id
  LEFT JOIN tag ON entry_tag.tag_id = tag.tag_id
  WHERE entry.created_at = (SELECT MAX(created_at) FROM entry);
