USE journal;

INSERT INTO author (full_name, age, country , email)
VALUES ('Dewald Els',25,'South Africa', 'dewald@gmail.com');

INSERT INTO author (full_name, age, country , email)
VALUES ('Donald T',75,'USA', 'makeUSAgreatagain@gmail.com');

INSERT INTO author (full_name, age, country , email)
VALUES ('Vladimir P',65,'Mother Russia', 'imbetterthanchucknorris@gmail.com');

INSERT INTO journal (title, author_name, category)
VALUES ('Nature','Dewald Els', 'science');

INSERT INTO journal (title, author_name, category)
VALUES ('Comicooon','Donald T', 'science');

INSERT INTO journal (title, author_name, category)
VALUES ('The earth is flat','Vladimir P', 'science');

INSERT INTO entry (title, author_id, journal_id, description_text)
VALUES ('Why the earth is flat',
	    (SELECT author_id FROM author WHERE full_name = 'Vladimir P'),
        (SELECT journal_id FROM journal WHERE title = 'The earth is flat')
        ,'Lorem');

INSERT INTO entry (title, author_id, journal_id, description_text)
VALUES ('The effects on society of the earth being flat',
		(SELECT author_id FROM author WHERE full_name = 'Vladimir P'),
        (SELECT journal_id FROM journal WHERE title = 'The earth is flat')
		,'Lorem ipsum');

INSERT INTO entry (title, author_id, journal_id, description_text)
VALUES ('How to fight the coronavirus',
		(SELECT author_id FROM author WHERE full_name = 'Vladimir P'),
        (SELECT journal_id FROM journal WHERE title = 'The earth is flat')
		,'Lorem ipsum fortis fortuna adiuvat');

INSERT INTO tag (title)
VALUES ('index.js');

INSERT INTO tag (title)
VALUES ('index.html');

INSERT INTO tag (title)
VALUES ('styles.css');

INSERT INTO entry_tag (entry_id, tag_id)
VALUES ((SELECT entry_id FROM entry WHERE title = 'Why the earth is flat'),
        (SELECT tag_id FROM tag WHERE title = 'index.js')),
        ((SELECT entry_id FROM entry WHERE title = 'Why the earth is flat'),
        (SELECT tag_id FROM tag WHERE title = 'index.html')),
        ((SELECT entry_id FROM entry WHERE title = 'Why the earth is flat'),
        (SELECT tag_id FROM tag WHERE title = 'styles.css'));

INSERT INTO entry_tag (entry_id, tag_id)
VALUES ((SELECT entry_id FROM entry WHERE title = 'How to fight the coronavirus'),
        (SELECT tag_id FROM tag WHERE title = 'index.js')),
        ((SELECT entry_id FROM entry WHERE title = 'How to fight the coronavirus'),
        (SELECT tag_id FROM tag WHERE title = 'index.html')),
        ((SELECT entry_id FROM entry WHERE title = 'How to fight the coronavirus'),
        (SELECT tag_id FROM tag WHERE title = 'styles.css'));
        
INSERT INTO entry_tag (entry_id, tag_id)
VALUES ((SELECT entry_id FROM entry WHERE title = 'The effects on society of the earth being flat'),
        (SELECT tag_id FROM tag WHERE title = 'index.js')),
        ((SELECT entry_id FROM entry WHERE title = 'The effects on society of the earth being flat'),
        (SELECT tag_id FROM tag WHERE title = 'index.html')),
        ((SELECT entry_id FROM entry WHERE title = 'The effects on society of the earth being flat'),
        (SELECT tag_id FROM tag WHERE title = 'styles.css'));
        





